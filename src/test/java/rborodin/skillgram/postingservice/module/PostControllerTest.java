package rborodin.skillgram.postingservice.module;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.jaegertracing.testcontainers.JaegerAllInOne;
import io.minio.MinioClient;
import io.opentracing.Tracer;
import org.instancio.Instancio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.mock.web.MockPart;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.MinIOContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import rborodin.skillgram.postingservice.dto.PostDTO;
import rborodin.skillgram.postingservice.helper.JsonHelper;
import rborodin.skillgram.postingservice.userservice.api.endpoints.UserserviceEndpoints;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.instancio.Select.field;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static rborodin.skillgram.postingservice.helper.JsonHelper.fromJson;

/**
 * Класс для модульного тестирования раздела постов
 */

@AutoConfigureMockMvc()
@AutoConfigureWireMock(port = 8181)
@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PostControllerTest {
    WireMockServer wireMockServer;


    @Autowired
    MockMvc mockMvc;
    @Container
    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:15");
    @Container
    static MinIOContainer minIOContainer = new MinIOContainer("minio/minio");
    @Container
    static JaegerAllInOne jaegerContainer = new JaegerAllInOne("jaegertracing/all-in-one:1.38");
    private static final String SERVICE_NAME = "query-test";
    private static Tracer tracer;

    /**
     * Настройка Testcontainer Postgres
     */
    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgres::getJdbcUrl);
        registry.add("spring.datasource.username", postgres::getUsername);
        registry.add("spring.datasource.password", postgres::getPassword);
        registry.add("minio.url", minIOContainer::getS3URL);
        registry.add("aws.s3.endpoint" , minIOContainer::getS3URL);
        registry.add("aws.s3.access-key", minIOContainer::getUserName);
        registry.add("aws.s3.secret-key", minIOContainer::getPassword);
        registry.add("userservice.uri", () -> "http://127.0.0.1");
    }


    @BeforeAll
    static void beforeAll() {
        postgres.start();
        minIOContainer.start();
        jaegerContainer.start();

    }

    @BeforeEach
    void setUp() {

        tracer = jaegerContainer.createTracer(SERVICE_NAME);

    }
    @Test
    void createPost() throws Exception {

        var post = Instancio.of(PostDTO.class)
                .ignore(field(PostDTO::getId))
                .ignore(field(PostDTO::getCreatedAt))
                .create();
        post.setPhotos(new ArrayList<>());
        var coverPart = new MockMultipartFile("photos", "minio.png", MediaType.IMAGE_PNG_VALUE, Files.readAllBytes(Paths.get("images/minio.png")));
        var postMockPart = new MockPart("post", JsonHelper.toJson(post).getBytes());
        postMockPart.getHeaders().setContentType(MediaType.APPLICATION_JSON);
        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo(UserserviceEndpoints.USERS + "/" + post.getUserUuid())).willReturn(WireMock.aResponse().withStatus(200)));

        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/posts")
                        .file(coverPart)
                        .part(postMockPart))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(UTF_8);
        Assertions.assertNotNull(fromJson(PostDTO.class, result));

        var postDTOResult = fromJson(PostDTO.class, mockMvc.perform(MockMvcRequestBuilders.get("/posts/"+fromJson(PostDTO.class, result).getId()))
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(UTF_8));
        Assertions.assertEquals(fromJson(PostDTO.class, result), postDTOResult);

    }

}