package rborodin.skillgram.postingservice.unit;

import org.instancio.Instancio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import org.springframework.web.server.ResponseStatusException;
import rborodin.skillgram.postingservice.dto.PostDTO;
import rborodin.skillgram.postingservice.entity.Photo;
import rborodin.skillgram.postingservice.entity.Post;
import rborodin.skillgram.postingservice.mapper.PhotoMapper;
import rborodin.skillgram.postingservice.mapper.PostMapper;
import rborodin.skillgram.postingservice.repository.PhotoRepository;
import rborodin.skillgram.postingservice.repository.PostRepository;
import rborodin.skillgram.postingservice.service.PostService;
import rborodin.skillgram.postingservice.userservice.api.UserserviceApiClient;

import javax.persistence.PersistenceException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class PostServiceTest {

    PostService postService;

    PhotoRepository photoRepository;

    PostRepository postRepository;

    UserserviceApiClient userserviceApiClient;


    @BeforeEach
    void setUp() {
        postRepository = mock(PostRepository.class);
        photoRepository = mock(PhotoRepository.class);
        userserviceApiClient = mock(UserserviceApiClient.class);
        postService = new PostService(postRepository, photoRepository, userserviceApiClient);
    }

    @Test
    void createPost() {

        //given
        Post post = Instancio.of(Post.class).create();
        List<Photo> photo = Instancio.ofList(Photo.class).create();
        when(postRepository.save(any(Post.class))).thenReturn(post);
        when(userserviceApiClient.checkUserExistsById(post.getUserUuid())).thenReturn(true);
        photo.forEach(x -> when(photoRepository.save(x)).thenReturn(x));

        //when
        PostDTO postDTO = postService.createPost(PostMapper.INSTANCE.toDto(post), PhotoMapper.INSTANCE.toDTOs(photo));

        //then
        Assertions.assertEquals(post.getId().toString(), postDTO.getId().toString());

    }

    @Test
    void createPostError() {

        //given
        Post post = Instancio.of(Post.class).create();
        List<Photo> photo = Instancio.ofList(Photo.class).create();
        when(userserviceApiClient.checkUserExistsById(post.getUserUuid())).thenReturn(true);
        when(postRepository.save(any(Post.class))).thenThrow(PersistenceException.class);
        //when
        Executable executable = () -> postService.createPost(PostMapper.INSTANCE.toDto(post),PhotoMapper.INSTANCE.toDTOs(photo));
        //then
        Assertions.assertThrows(PersistenceException.class, executable);
    }

    @Test
    void createPostUserNotFound() {

        //given
        Post post = Instancio.of(Post.class).create();
        List<Photo> photo = Instancio.ofList(Photo.class).create();
        when(userserviceApiClient.checkUserExistsById(post.getUserUuid())).thenReturn(false);
        //when
        Executable executable = () -> postService.createPost(PostMapper.INSTANCE.toDto(post),PhotoMapper.INSTANCE.toDTOs(photo));
        //then
        Assertions.assertThrows(ResponseStatusException.class, executable);
    }

    @Test
    void findById() {

        //given
        Post post = Instancio.of(Post.class).create();
        when(postRepository.findById(post.getId())).thenReturn(Optional.of(post));

        //when
        String result = String.valueOf(postService.findById(post.getId()));

        //then
        Assertions.assertEquals(PostMapper.INSTANCE.toDto(post).toString(), result);
    }

    @Test
    void findByIdNotFound() {

        //given
        Post post = Instancio.of(Post.class).create();
        when(postRepository.findById(any(UUID.class))).thenReturn(Optional.empty());

        //when
        Executable executable = () -> postService.findById(post.getId());

        //then
        Assertions.assertThrows(ResponseStatusException.class, executable);
    }

    @Test
    void updatePost() {

        //given
        Post post = Instancio.of(Post.class).create();
        List<Photo> photo = Instancio.ofList(Photo.class).create();
        when(postRepository.save(any(Post.class))).thenReturn(post);
        when(postRepository.existsById(any(UUID.class))).thenReturn(true);
        when(postRepository.findById(any(UUID.class))).thenReturn(Optional.ofNullable(post));

        when(userserviceApiClient.checkUserExistsById(post.getUserUuid())).thenReturn(true);
        photo.forEach(x -> when(photoRepository.save(x)).thenReturn(x));

        //when
        PostDTO postDTO = postService.updatePost(PostMapper.INSTANCE.toDto(post),post.getId(), PhotoMapper.INSTANCE.toDTOs(photo));

        //then
        Assertions.assertEquals(post.getId().toString(), postDTO.getId().toString());
    }

    @Test
    void updatePostNotFound() {

        //given
        Post post = Instancio.of(Post.class).create();
        List<Photo> photos = Instancio.ofList(Photo.class).create();
        when(postRepository.existsById(any(UUID.class))).thenReturn(false);

        //when
        Executable executable = () -> postService.updatePost(PostMapper.INSTANCE.toDto(post),post.getId(), PhotoMapper.INSTANCE.toDTOs(photos));

        //then
        Assertions.assertThrows(ResponseStatusException.class, executable);
    }

    @Test
    void deletePost() {

        //given
        Post post = Instancio.of(Post.class).create();

        when(postRepository.existsById(any(UUID.class))).thenReturn(true);
        when(postRepository.findById(post.getId())).thenReturn(Optional.of(post));

        //when
        String result = postService.deletePost(post.getId());
        Mockito.verify(postRepository, times(1))
                .deleteById(post.getId());
        //then
        Assertions.assertEquals(String.format("Пост с id=%s успешно удален", post.getId()), result);
    }

    @Test
    void deletePostNotFound() {
        //given
        Post post = Instancio.of(Post.class).create();

        when(postRepository.existsById(any(UUID.class))).thenReturn(false);

        //when
        Executable executable = () -> postService.deletePost(post.getId());
        Mockito.verify(postRepository, times(0))
                .deleteById(post.getId());
        //then
        Assertions.assertThrows(ResponseStatusException.class, executable);

    }

    @Test
    void findAllByUserUuid() {
        //given
        List<Post> posts = Instancio.ofList(Post.class).create();
        when(postRepository.findAllByUserUuid(any(UUID.class))).thenReturn(posts);

        //when
        List<PostDTO> result = postService.findAllByUserUuid(posts.get(0).getUserUuid());
        //then
        Mockito.verify(postRepository, times(1))
                .findAllByUserUuid(any(UUID.class));
        Assertions.assertEquals(PostMapper.INSTANCE.toDTOs(posts), result);
    }

    @Test
    void findAllByUserUuidNotFound() {
        //given
        List<Post> posts = Instancio.ofList(Post.class).create();
        when(postRepository.findAllByUserUuid(any(UUID.class))).thenReturn(Collections.emptyList());

        //when
        List<PostDTO> result = postService.findAllByUserUuid(posts.get(0).getUserUuid());
        //then
        Mockito.verify(postRepository, times(1))
                .findAllByUserUuid(any(UUID.class));
        Assertions.assertEquals(0,result.size());
    }


}