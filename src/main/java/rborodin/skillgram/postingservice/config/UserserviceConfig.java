package rborodin.skillgram.postingservice.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix="userservice")
public class UserserviceConfig {
    private String uri;
    private Integer port;
    public void setUri(String uri) {
        this.uri = uri;
    }
    public void setPort(Integer port) {
        this.port = port;
    }
}
