package rborodin.skillgram.postingservice.exception;

public class NotFoundException extends AppException {

    public NotFoundException(String message) {
        super(message);
    }

}