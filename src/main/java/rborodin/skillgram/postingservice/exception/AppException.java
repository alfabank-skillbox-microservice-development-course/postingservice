package rborodin.skillgram.postingservice.exception;

public class AppException extends RuntimeException {

    public AppException(String message) {
        super(message);
    }
}