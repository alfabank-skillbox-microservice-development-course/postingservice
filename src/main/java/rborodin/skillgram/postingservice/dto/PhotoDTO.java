package rborodin.skillgram.postingservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.UUID;

@Data
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public class PhotoDTO {
    private UUID id;
    private String link;
    private String name;
}

