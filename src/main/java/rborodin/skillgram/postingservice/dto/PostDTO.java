package rborodin.skillgram.postingservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public class PostDTO {
    private UUID id;
    private String title;
    private UUID userUuid;
    private String descriptions;
    private List<PhotoDTO> photos;
    private Timestamp createdAt;
}
