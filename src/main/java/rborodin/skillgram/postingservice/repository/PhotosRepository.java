package rborodin.skillgram.postingservice.repository;

import com.amazonaws.services.s3.AmazonS3;
import org.springframework.stereotype.Component;
import rborodin.skillgram.postingservice.propertires.S3Properties;

@Component
public class PhotosRepository extends S3Repository {
    public PhotosRepository(AmazonS3 s3Client, S3Properties properties) {
        super(s3Client, properties.getBucketName());
    }

}
