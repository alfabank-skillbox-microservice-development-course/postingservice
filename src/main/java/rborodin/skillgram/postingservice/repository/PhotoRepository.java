package rborodin.skillgram.postingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rborodin.skillgram.postingservice.entity.Photo;

import java.util.List;
import java.util.UUID;

public interface PhotoRepository extends JpaRepository<Photo, UUID> {
    List<Photo> findAllByPost_Id(UUID uuid);
}
