package rborodin.skillgram.postingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rborodin.skillgram.postingservice.entity.Post;

import java.util.List;
import java.util.UUID;

public interface PostRepository  extends JpaRepository<Post, UUID> {

    List<Post> findAllByUserUuid(UUID userUuid);
}
