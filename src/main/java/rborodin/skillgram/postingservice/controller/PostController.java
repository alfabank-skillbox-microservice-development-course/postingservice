package rborodin.skillgram.postingservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rborodin.skillgram.postingservice.dto.PhotoDTO;
import rborodin.skillgram.postingservice.dto.PostDTO;
import rborodin.skillgram.postingservice.service.PhotosService;
import rborodin.skillgram.postingservice.service.PostService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "/posts")
public class PostController {
    private final PostService postService;
    private final PhotosService photosService;

    public PostController(PostService postService, PhotosService photosService) {
        this.postService = postService;
        this.photosService = photosService;
    }

    @Operation(summary = "Добавление поста и фотографий к посту")
    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    PostDTO createPost(@RequestPart("post") PostDTO post, @RequestPart("photos") MultipartFile[] photos) throws IOException {
        log.info("Получено событие добавления поста и фотографий к посту");
        List<PhotoDTO> photoDTOS = photosService.uploadPhoto(photos);
        return postService.createPost(post, photoDTOS);
    }

    @Operation(summary = "Получение поста и фотографий к посту")
    @GetMapping("/{post_uuid}")
    PostDTO getPostById(@PathVariable("post_uuid") UUID id) {
        log.info("Получено событие получения поста и фотографий к посту по id {}", id);
        return postService.findById(id);
    }

    @Operation(summary = "Обновление поста и фотографий к посту")
    @PutMapping(path = "/{post_uuid}", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    PostDTO updatePost(@RequestPart("post") PostDTO post, @RequestPart("photos") MultipartFile[] photos, @PathVariable("post_uuid") UUID post_uuid) throws IOException {
        log.info("Получено событие обновления поста и фотографий к посту по id {}", post_uuid);
        List<PhotoDTO> photoDTOS = postService.findById(post_uuid).getPhotos();
        photoDTOS.forEach(x -> photosService.deletePhoto(x.getName()));
        photoDTOS = photosService.uploadPhoto(photos);
        return postService.updatePost(post, post_uuid, photoDTOS);
    }

    @Operation(summary = "Удаление поста и фотографий к посту")
    @DeleteMapping("/{post_uuid}")
    String deletePost(@PathVariable UUID post_uuid) {
        log.info("Получено событие удаления поста и фотографий к посту по id {}", post_uuid);
        List<PhotoDTO> photoDTOS = postService.findById(post_uuid).getPhotos();
        photoDTOS.forEach(x -> photosService.deletePhoto(x.getName()));
        return postService.deletePost(post_uuid);
    }

    @Operation(summary = "Получение всех постов пользователя")
    @GetMapping()
    List<PostDTO> findAllPosts(@RequestParam UUID user_uuid) {
        log.info("Получено событие получения всех постов пользователя по user_uuid {}", user_uuid);
        return new ArrayList<>(postService.findAllByUserUuid(user_uuid));
    }

}
