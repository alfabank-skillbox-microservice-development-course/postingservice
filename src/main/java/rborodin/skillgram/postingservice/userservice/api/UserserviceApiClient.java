package rborodin.skillgram.postingservice.userservice.api;

import io.restassured.RestAssured;
import org.springframework.stereotype.Component;
import rborodin.skillgram.postingservice.config.UserserviceConfig;
import rborodin.skillgram.postingservice.userservice.api.endpoints.UserserviceEndpoints;
import rborodin.skillgram.postingservice.userservice.api.specifications.UserserviceSpecifications;

import java.util.UUID;

@Component
public class UserserviceApiClient {
    private final UserserviceSpecifications userserviceSpecifications;

    UserserviceConfig userserviceConfig;

    public UserserviceApiClient(UserserviceConfig userserviceConfig,UserserviceSpecifications userserviceSpecifications) {
        this.userserviceConfig = userserviceConfig;
        this.userserviceSpecifications = userserviceSpecifications;
    }

    public boolean checkUserExistsById(UUID id) {
        return RestAssured.given()
                .spec(userserviceSpecifications.getRequestSpec())
                .pathParam("id", id)
                .log().all()
                .when()
                .get(userserviceConfig.getUri() + ":" + userserviceConfig.getPort() + UserserviceEndpoints.USERS + "/{id}")
                .then()
                .log().body()
                .extract().response().getStatusCode() == 200;
    }


}
