package rborodin.skillgram.postingservice.userservice.api.endpoints;

public class UserserviceEndpoints {
public static final String USERS = "/users";

    private UserserviceEndpoints() {
        throw new IllegalStateException("Utility class");
    }
}
