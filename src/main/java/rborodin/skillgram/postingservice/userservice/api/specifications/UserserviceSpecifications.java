package rborodin.skillgram.postingservice.userservice.api.specifications;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class UserserviceSpecifications {



    private RequestSpecification requestSpec = new RequestSpecBuilder()
            .setAccept(ContentType.JSON)
            .setContentType(ContentType.JSON)
            .log(LogDetail.ALL)
            .build();


    private ResponseSpecification responseSpec = new ResponseSpecBuilder()
            .log(LogDetail.ALL)
            .expectStatusCode(200)
            .build();

    private ResponseSpecification responseSpec404 = new ResponseSpecBuilder()
            .log(LogDetail.ALL)
            .expectStatusCode(404)
            .build();

}
