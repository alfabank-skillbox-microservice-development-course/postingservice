package rborodin.skillgram.postingservice.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import rborodin.skillgram.postingservice.dto.PhotoDTO;
import rborodin.skillgram.postingservice.exception.AppException;
import rborodin.skillgram.postingservice.propertires.S3Properties;
import rborodin.skillgram.postingservice.repository.PhotosRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class PhotosService {

    private final PhotosRepository photosRepository;

    private final S3Properties properties;

    public List<PhotoDTO> uploadPhoto( MultipartFile[] photos) throws IOException {
        List<PhotoDTO> photoDTOS = new ArrayList<>();

        if (photos[0].getSize() != 0) {
            for (MultipartFile photo : photos) {
                try (InputStream stream = new ByteArrayInputStream(photo.getBytes())) {
                    log.info("Uploading {} to bucket {} in S3", photo.getOriginalFilename(), properties.getBucketName());
                    ObjectMetadata metadata = new ObjectMetadata();
                    metadata.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
                    metadata.setContentLength(photo.getSize());
                    photosRepository.put(photo.getOriginalFilename(), stream, metadata);
                    log.info("Photo '{}' was successfully uploaded", photo.getOriginalFilename());
                    PhotoDTO photoDTO = new PhotoDTO();
                    photoDTO.setLink(photosRepository.getURL(photo.getOriginalFilename()).toString());
                    photoDTO.setName(photosRepository.get(photo.getOriginalFilename()).get().getKey());
                    photoDTOS.add(photoDTO);
                } catch (AmazonServiceException e) {
                    throw new AppException(e.getMessage());
                }
            }
        }
       return photoDTOS;
    }

    public void deletePhoto(String fileName) {
        String bucketName = properties.getBucketName();
        log.info("Removing {} from bucket {} in S3", fileName, bucketName);
        if (photosRepository.listKeys(fileName).isEmpty()) {
            log.error("Photo {} have not deleted", fileName);
        }
        photosRepository.delete(fileName);

        log.info("Photo '{}' was successfully deleted", fileName);
        log.info("Photo {} was successfully deleted", fileName);
    }

    public InputStream readPhoto(String key) {
        return photosRepository.get(key)
                .map(S3Object::getObjectContent)
                .orElseThrow(() -> new IllegalStateException("Object not found " + key));
    }
}
