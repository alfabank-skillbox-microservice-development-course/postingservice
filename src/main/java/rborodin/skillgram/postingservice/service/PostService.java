package rborodin.skillgram.postingservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import rborodin.skillgram.postingservice.dto.PhotoDTO;
import rborodin.skillgram.postingservice.dto.PostDTO;
import rborodin.skillgram.postingservice.entity.Photo;
import rborodin.skillgram.postingservice.entity.Post;
import rborodin.skillgram.postingservice.exception.NotFoundException;
import rborodin.skillgram.postingservice.mapper.PhotoMapper;
import rborodin.skillgram.postingservice.mapper.PostMapper;
import rborodin.skillgram.postingservice.repository.PhotoRepository;
import rborodin.skillgram.postingservice.repository.PostRepository;
import rborodin.skillgram.postingservice.userservice.api.UserserviceApiClient;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class PostService {

    private final PostRepository postRepository;
    private final PhotoRepository photoRepository;
    UserserviceApiClient userserviceApiClient;


    public PostService(PostRepository postRepository, PhotoRepository photoRepository, UserserviceApiClient userserviceApiClient) {
        this.postRepository = postRepository;
        this.photoRepository = photoRepository;
        this.userserviceApiClient = userserviceApiClient;
    }

    @NewSpan
    public PostDTO createPost(PostDTO postDTO, List<PhotoDTO> photoDTOS) {
        log.info("Проверка наличия пользователя в сервисе пользователей по id {}", postDTO.getUserUuid());
        if (!userserviceApiClient.checkUserExistsById(postDTO.getUserUuid())) {
            throw new NotFoundException("Отсутствует пользователь с id = " + postDTO.getUserUuid());
        } else{
            log.info("Пользователь с id {} успешно найден", postDTO.getUserUuid());
        }
        postDTO.setCreatedAt(Timestamp.from(Instant.now()));
        Post postToAdd = PostMapper.INSTANCE.toEntity(postDTO);
        postToAdd = postRepository.save(postToAdd);
        List<PhotoDTO> savedPhotos = new ArrayList<>();
        for (PhotoDTO photoDTO : photoDTOS) {
            Photo photo = PhotoMapper.INSTANCE.toEntity(photoDTO);
            photo.setPost(postToAdd);
            photo = photoRepository.save(photo);
            savedPhotos.add(PhotoMapper.INSTANCE.toDto(photo));
        }
        PostDTO addedPost = PostMapper.INSTANCE.toDto(postToAdd);
        addedPost.setPhotos(savedPhotos);
        return addedPost;
    }

    @NewSpan
    public PostDTO findById(UUID id) {
        return PostMapper.INSTANCE.toDto(postRepository.findById(id).orElseThrow(() -> new NotFoundException("Отсутствует пост с id = " + id)));
    }

    @NewSpan
    public PostDTO updatePost(PostDTO postDTO, UUID id, List<PhotoDTO> photoDTOS) {

        if (postRepository.existsById(id)) {

            Post postToUpdate = postRepository.findById(id).orElseThrow(() -> new NotFoundException("Отсутствует пост с id = " + id));
            postToUpdate.setTitle(postDTO.getTitle());
            postToUpdate.setDescriptions(postDTO.getDescriptions());
            if (!userserviceApiClient.checkUserExistsById(postDTO.getUserUuid())) {
                throw new NotFoundException("Отсутствует пользователь с id = " + postDTO.getUserUuid());
            } else {
                postToUpdate.setUserUuid(postDTO.getUserUuid());
            }
            List<Photo> savedPhotos = new ArrayList<>();
            for (PhotoDTO photoDTO : photoDTOS) {
                Photo photo = PhotoMapper.INSTANCE.toEntity(photoDTO);
                photo.setPost(postToUpdate);
                savedPhotos.add(photo);
            }
            postToUpdate.getPhotos().clear();
            postToUpdate.getPhotos().addAll(savedPhotos);
            postRepository.saveAndFlush(postToUpdate);
            return PostMapper.INSTANCE.toDto(postToUpdate);
        } else throw new NotFoundException("Отсутствует пост с id = " + id);
    }

    @NewSpan
    public String deletePost(UUID id) {
        if (postRepository.existsById(id)) {
            postRepository.deleteById(id);
        } else throw new NotFoundException("Отсутствует пост с id = " + id);
        return String.format("Пост с id=%s успешно удален", id);
    }

    public List<PostDTO> findAllByUserUuid(UUID uuid) {
        return PostMapper.INSTANCE.toDTOs(new ArrayList<>(postRepository.findAllByUserUuid(uuid)));
    }

}
