package rborodin.skillgram.postingservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import rborodin.skillgram.postingservice.dto.PostDTO;
import rborodin.skillgram.postingservice.entity.Post;

import java.util.List;

@Mapper
public interface PostMapper {

    PostMapper INSTANCE = Mappers.getMapper(PostMapper.class);
    PostDTO toDto(Post post);
    Post toEntity(PostDTO postDTO);
    List<PostDTO> toDTOs(List<Post> posts);

}
