package rborodin.skillgram.postingservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import rborodin.skillgram.postingservice.dto.PhotoDTO;
import rborodin.skillgram.postingservice.entity.Photo;

import java.util.List;

@Mapper
public interface PhotoMapper {

    PhotoMapper INSTANCE = Mappers.getMapper(PhotoMapper.class);
    PhotoDTO toDto(Photo photo);
    Photo toEntity(PhotoDTO photoDTO);
    List<Photo>  toEntities(List<PhotoDTO> photoDTOs);

    List<PhotoDTO> toDTOs(List<Photo> photos);

}
